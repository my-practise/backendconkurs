package com.grant.freeGrant.applicants.repository;


import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.other.model.Status;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ApplicantRepository extends JpaRepository<Applicant, Long> {
    @Query("SELECT a FROM Applicant a WHERE a.vacantGrant = :vacantGrantId")
    List<Applicant> findByVacantGrantId(VacantGrant vacantGrantId);
}
