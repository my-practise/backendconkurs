package com.grant.freeGrant.applicants.service;

import com.grant.freeGrant.applicants.dto.ApplicantSaveRequest;
import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.applicants.repository.ApplicantRepository;
import com.grant.freeGrant.other.model.Approving;
import com.grant.freeGrant.other.repository.ApprovingRepository;
import com.grant.freeGrant.other.request.ApprovingSaveRequest;
import com.grant.freeGrant.other.service.ApprovingService;
import com.grant.freeGrant.other.service.EmailService;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import com.grant.freeGrant.vacantGrant.repository.VacantGrantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicantService {
    private final ApplicantRepository repository;
    private final VacantGrantRepository vacantGrantRepository;
    private final ApprovingService approvingService;
    private final ApprovingRepository approvingRepository;
    private final EmailService emailService;

    @Transactional
    public Applicant saveApplicant(ApplicantSaveRequest request) {
        VacantGrant vacantGrant = vacantGrantRepository.findById(request.getVacantGrantId()).orElseThrow(() -> new RuntimeException("Status not found"));
        Applicant applicant = new Applicant();
        applicant.setCreateTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        applicant.setGpa(request.getGpa());
        applicant.setIin(request.getIin());
        applicant.setFirstName(request.getFirstName());
        applicant.setLastName(request.getLastName());
        applicant.setUserId(request.getUserId());
        applicant.setVacantGrant(vacantGrant);

        Applicant loadApplicant = repository.save(applicant);

        if (!request.getApproving().isEmpty()){
            for (ApprovingSaveRequest saveRequest : request.getApproving()){
                Approving approving = new Approving();
                approving.setApplicantId(loadApplicant);
                approving.setAgree(false);
                approving.setEmail(saveRequest.getEmail());
                approving.setFullName(saveRequest.getFullName());
                approvingService.saveApproving(approving);
            }

            String fullName = loadApplicant.getFirstName() + " " + loadApplicant.getLastName();
            List<Approving> saveApprovings = approvingRepository.findByApplicant(loadApplicant);
            emailService.sendSimpleMessage(saveApprovings, fullName);
        }
        return loadApplicant;
    }

    public List<Applicant> listAllApplicants() {
        return repository.findAll();
    }

    public Applicant findApplicantById(Long id) {
        return repository.findById(id).orElse(null);
    }
}