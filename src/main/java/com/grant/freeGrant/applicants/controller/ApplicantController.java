package com.grant.freeGrant.applicants.controller;

import com.grant.freeGrant.applicants.dto.ApplicantSaveRequest;
import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.applicants.service.ApplicantService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/applicants")
@RequiredArgsConstructor
public class ApplicantController {
    private final ApplicantService service;

    @PostMapping("/save")
    public ResponseEntity<Applicant> saveApplicant(@RequestBody ApplicantSaveRequest request) {
        return ResponseEntity.ok(service.saveApplicant(request));
    }

    @GetMapping("/listAll")
    public ResponseEntity<List<Applicant>> listAllApplicants() {
        return ResponseEntity.ok(service.listAllApplicants());
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Applicant> findApplicantById(@PathVariable Long id) {
        Applicant applicant = service.findApplicantById(id);
        return applicant != null ? ResponseEntity.ok(applicant) : ResponseEntity.notFound().build();
    }
}