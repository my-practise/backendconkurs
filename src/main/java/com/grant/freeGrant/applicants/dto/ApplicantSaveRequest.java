package com.grant.freeGrant.applicants.dto;

import com.grant.freeGrant.other.request.ApprovingSaveRequest;
import lombok.Data;

import java.util.List;

@Data
public class ApplicantSaveRequest {
    private Long vacantGrantId;
    private String firstName;
    private String lastName;
    private String userId;
    private String iin;
    private Double gpa;
    private List<ApprovingSaveRequest> approving;
}
