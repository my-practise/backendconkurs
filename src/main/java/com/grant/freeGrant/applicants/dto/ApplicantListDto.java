package com.grant.freeGrant.applicants.dto;

import lombok.Data;

@Data
public class ApplicantListDto {
    private Long vacantGrantId;
    private Long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String iin;
    private Double gpa;

    public ApplicantListDto(Long vacantGrantId, Long id, String userId, String firstName, String lastName, String iin, Double gpa) {
        this.vacantGrantId = vacantGrantId;
        this.id = id;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.iin = iin;
        this.gpa = gpa;
    }
}
