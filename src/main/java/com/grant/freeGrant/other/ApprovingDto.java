package com.grant.freeGrant.other;

import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import lombok.Data;

@Data
public class ApprovingDto {
    private Long approvingId;
    private String fullName;
    private Boolean isAgree;

    private Long vacantGrantId;
    private String specializationName;
    private String facultyName;
    private String course;

    private Long applicantId;
    private String applicantFirstName;
    private String applicantLastName;
}
