package com.grant.freeGrant.other.repository;

import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.other.model.Approving;
import com.grant.freeGrant.other.model.Course;
import com.grant.freeGrant.other.model.CourseType;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ApprovingRepository extends JpaRepository<Approving, Long> {
    @Modifying
    @Query("UPDATE Approving a SET a.isAgree = :isAgree WHERE a.id = :id")
    int updateIsAgreeById(Long id, boolean isAgree);

    @Query("SELECT count(ap.isAgree) / 2.0 FROM Approving ap WHERE ap.applicantId = :id AND ap.isAgree = true")
    Double getCountIsAgree(Applicant id);

    @Query("SELECT a FROM Approving a WHERE a.applicantId = :applicant")
    List<Approving> findByApplicant(@Param("applicant") Applicant applicant);
}