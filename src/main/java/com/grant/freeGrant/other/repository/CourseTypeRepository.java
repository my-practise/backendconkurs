package com.grant.freeGrant.other.repository;

import com.grant.freeGrant.other.model.Course;
import com.grant.freeGrant.other.model.CourseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CourseTypeRepository extends JpaRepository<CourseType, Long> {
    @Query("SELECT ct FROM CourseType ct WHERE ct.course = :course")
    List<CourseType> findByCourse(Course course);
}