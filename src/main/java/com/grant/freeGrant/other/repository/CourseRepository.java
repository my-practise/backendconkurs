package com.grant.freeGrant.other.repository;

import com.grant.freeGrant.other.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
