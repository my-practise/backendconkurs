package com.grant.freeGrant.other.repository;


import com.grant.freeGrant.other.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface StatusRepository extends JpaRepository<Status, Long> {
    @Query("SELECT s.statusId FROM Status s WHERE s.statusName = :statusName")
    Long findByStatusName(String statusName);
}
