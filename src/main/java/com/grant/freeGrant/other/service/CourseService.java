package com.grant.freeGrant.other.service;

import com.grant.freeGrant.other.model.Course;
import com.grant.freeGrant.other.repository.CourseRepository;
import com.grant.freeGrant.other.request.CourseSaveRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;

    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    public void createCourse(CourseSaveRequest request) {
        courseRepository.save(new Course(request.getCourseName()));
    }
}