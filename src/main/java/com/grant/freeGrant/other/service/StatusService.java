package com.grant.freeGrant.other.service;

import com.grant.freeGrant.other.request.StatusSaveRequest;
import com.grant.freeGrant.other.model.Status;
import com.grant.freeGrant.other.repository.StatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StatusService {
    private final StatusRepository statusRepository;

    public void save(StatusSaveRequest request){
        Long statusId = statusRepository.findByStatusName(request.getStatusName());
        if (statusId != null){
            throw new IllegalArgumentException("С таким именем поля существует!");
        }
        statusRepository.save(new Status(request.getStatusName()));
    }

    public List<Status> listAll(){
        return statusRepository.findAll();
    }
}
