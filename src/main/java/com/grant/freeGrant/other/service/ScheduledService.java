package com.grant.freeGrant.other.service;

import com.grant.freeGrant.result.service.ResultService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduledService {
    private final ResultService service;

    @Scheduled(cron = "0 0 0 * * ?") // Запускается в начале каждого дня
    public void reportCurrentTimeWithCron() {
        service.resultSave();
    }

}
