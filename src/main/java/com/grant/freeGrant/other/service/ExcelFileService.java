package com.grant.freeGrant.other.service;

import com.grant.freeGrant.result.model.Result;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelFileService {

    public ByteArrayInputStream exportToExcel(List<Result> results) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet("Users");

            // Создание заголовка
            Row headerRow = sheet.createRow(0);
            headerRow.createCell(0).setCellValue("ID");
            headerRow.createCell(1).setCellValue("Полное имя участника");
            headerRow.createCell(2).setCellValue("Балл");
            headerRow.createCell(3).setCellValue("Время");

            // Заполнение данных
            int rowIdx = 1;
            for (Result result : results) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(rowIdx);
                row.createCell(1).setCellValue(result.getFullName());
                row.createCell(2).setCellValue(result.getScore());
                row.createCell(3).setCellValue(result.getDate());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("Ошибка при создании Excel файла: " + e.getMessage());
        }
    }
}