package com.grant.freeGrant.other.service;
import com.grant.freeGrant.applicants.dto.ApplicantSaveRequest;
import com.grant.freeGrant.other.model.Approving;
import com.grant.freeGrant.other.request.ApprovingSaveRequest;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmailService {
    private final JavaMailSender mailSender;

    @Async
    public void sendSimpleMessage(List<Approving> saveApprovings, String fullName) {
        for (Approving approving : saveApprovings) {
            String subject = "Вы подтверждаете студента с именем " + fullName;
            String link = "http://localhost:8080/approve_student?approving=" + approving.getId();
            String text = "Вы можете подтвердить переходя по этой <a href='" + link + "'>ссылке</a>.";

            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, "utf-8");

            try {
                helper.setFrom("ualiev.1120@gmail.com");
                helper.setTo(approving.getEmail());
                helper.setSubject(subject);
                helper.setText(text, true); // true indicates that this is an HTML email
            } catch (MessagingException e) {
                e.printStackTrace(); // Or any other error handling
            }

            mailSender.send(message);
        }
    }
}