package com.grant.freeGrant.other.service;

import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.applicants.repository.ApplicantRepository;
import com.grant.freeGrant.other.ApprovingDto;
import com.grant.freeGrant.other.model.Approving;
import com.grant.freeGrant.other.repository.ApprovingRepository;
import com.grant.freeGrant.other.request.IsAgreeRequest;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import com.grant.freeGrant.vacantGrant.repository.VacantGrantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ApprovingService {
    private final ApprovingRepository repository;
    private final ApplicantRepository applicantRepository;
    private final VacantGrantRepository vacantGrantRepository;

    public Approving saveApproving(Approving approving) {
        return repository.save(approving);
    }

    public List<Approving> listAllApprovings() {
        return repository.findAll();
    }

    public ApprovingDto findApprovingById(Long id) {
        ApprovingDto approvingDto = new ApprovingDto();
        Approving approving = repository.findById(id).orElse(null);
        Applicant applicant = applicantRepository.findById(approving.getApplicantId().getId()).orElse(null);
        VacantGrant vacantGrant = vacantGrantRepository.findById(applicant.getVacantGrant().getId()).orElse(null);

        approvingDto.setVacantGrantId(vacantGrant.getId());
        approvingDto.setFacultyName(vacantGrant.getSpecialization().getFaculty().getFacultyName());
        approvingDto.setCourse(vacantGrant.getCourse());
        approvingDto.setApplicantId(applicant.getId());
        approvingDto.setApplicantFirstName(applicant.getFirstName());
        approvingDto.setApplicantLastName(applicant.getLastName());
        approvingDto.setSpecializationName(vacantGrant.getSpecialization().getName());
        approvingDto.setApprovingId(approving.getId());
        approvingDto.setFullName(approving.getFullName());
        approvingDto.setIsAgree(approving.getAgree());

        return approvingDto;
    }

    @Transactional
    public void isAgree (IsAgreeRequest request) {
        int updatedCount = repository.updateIsAgreeById(request.getId(), true);
        if (updatedCount == 0) {
            throw new RuntimeException("Approving not found or update failed for id " + request.getId());
        }
    }
}