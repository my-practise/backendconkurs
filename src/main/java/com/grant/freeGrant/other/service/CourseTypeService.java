package com.grant.freeGrant.other.service;

import com.grant.freeGrant.other.model.Course;
import com.grant.freeGrant.other.model.CourseType;
import com.grant.freeGrant.other.repository.CourseTypeRepository;
import com.grant.freeGrant.other.request.CourseTypeSaveRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CourseTypeService {
    private final CourseTypeRepository courseTypeRepository;

    public List<CourseType> getAllCourseTypes() {
        return courseTypeRepository.findAll();
    }

    public void createCourseType(CourseTypeSaveRequest request) {
        courseTypeRepository.save(new CourseType(request.getCourse(), request.getTypeName()));
    }

    public List<CourseType> getAllCourseTypesByCourse(Course course) {
        return courseTypeRepository.findByCourse(course);
    }
}