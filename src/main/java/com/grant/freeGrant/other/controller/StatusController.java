package com.grant.freeGrant.other.controller;

import com.grant.freeGrant.other.model.Status;
import com.grant.freeGrant.other.request.StatusSaveRequest;
import com.grant.freeGrant.other.service.StatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@Controller
@RestController
@RequestMapping("/status")
public class StatusController {
    private final StatusService statusService;

    @PostMapping("/save")
    public void save(@RequestBody StatusSaveRequest request){
        statusService.save(request);
    }

    @PostMapping("listAll")
    public List<Status> listAll(){
        return statusService.listAll();
    }
}
