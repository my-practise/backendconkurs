package com.grant.freeGrant.other.controller;

import com.grant.freeGrant.other.model.Course;
import com.grant.freeGrant.other.request.CourseSaveRequest;
import com.grant.freeGrant.other.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@Controller
@RestController
@RequestMapping("/course")
public class CourseController {
    private final CourseService courseService;

    @PostMapping("/save")
    public void save(@RequestBody CourseSaveRequest request){
        courseService.createCourse(request);
    }

    @GetMapping("/listAll")
    public List<Course> listAll(){
        return courseService.getAllCourses();
    }
}
