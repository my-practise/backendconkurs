package com.grant.freeGrant.other.controller;

import com.grant.freeGrant.other.service.ExcelFileService;
import com.grant.freeGrant.result.model.Result;
import com.grant.freeGrant.result.service.ResultService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class FileDownloadController {
   private final ExcelFileService excelFileService;
   private final ResultService resultService;

    @GetMapping("/download/excel/{id}")
    public ResponseEntity<byte[]> downloadExcelFile(@PathVariable Long id) {
        List<Result> resultList = resultService.listAll(id);
        ByteArrayInputStream stream = excelFileService.exportToExcel(resultList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=results.xlsx");

        return ResponseEntity.ok()
                .headers(headers)
                .body(stream.readAllBytes());
    }
}