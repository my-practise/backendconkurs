package com.grant.freeGrant.other.controller;

import com.grant.freeGrant.other.model.Course;
import com.grant.freeGrant.other.model.CourseType;
import com.grant.freeGrant.other.request.CourseTypeSaveRequest;
import com.grant.freeGrant.other.service.CourseTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RequiredArgsConstructor
@Controller
@RestController
@RequestMapping("/courseType")
public class CourseTypeController {
    private final CourseTypeService courseTypeService;

    @PostMapping("/create")
    public void create(@RequestBody CourseTypeSaveRequest request){
        courseTypeService.createCourseType(request);
    }

    @GetMapping("/listAll")
    public List<CourseType> listAll(){
       return courseTypeService.getAllCourseTypes();
    }

    @PostMapping("/listAllByCourse")
    public List<CourseType> listAllByCourse(@RequestBody Course request){
        return courseTypeService.getAllCourseTypesByCourse(request);
    }
}
