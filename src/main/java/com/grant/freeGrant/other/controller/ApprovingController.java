package com.grant.freeGrant.other.controller;

import com.grant.freeGrant.other.ApprovingDto;
import com.grant.freeGrant.other.model.Approving;
import com.grant.freeGrant.other.request.IsAgreeRequest;
import com.grant.freeGrant.other.service.ApprovingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/approvings")
@RequiredArgsConstructor
public class ApprovingController {
    private final ApprovingService service;

    @PostMapping("/save")
    public ResponseEntity<Approving> saveApproving(@RequestBody Approving approving) {
        return ResponseEntity.ok(service.saveApproving(approving));
    }

    @GetMapping("/listAll")
    public ResponseEntity<List<Approving>> listAllApprovings() {
        return ResponseEntity.ok(service.listAllApprovings());
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<ApprovingDto> findApprovingById(@PathVariable Long id) {
        ApprovingDto approving = service.findApprovingById(id);
        return approving != null ? ResponseEntity.ok(approving) : ResponseEntity.notFound().build();
    }

    @PostMapping("/isAgree")
    public void isAgree(@RequestBody IsAgreeRequest request) {
        service.isAgree(request);
    }
}