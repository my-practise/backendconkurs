package com.grant.freeGrant.other;

public enum StatusEnum {
    NEW(1, "Новая"),
    COMPLETED(2, "Закончено");

    private final long id;
    private final String displayName;

    StatusEnum(int id, String displayName) {
        this.id = id;
        this.displayName = displayName;
    }

    public long getId() {
        return this.id;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
