package com.grant.freeGrant.other.request;

import lombok.Data;

@Data
public class CourseSaveRequest {
    private String courseName;
}
