package com.grant.freeGrant.other.request;

import lombok.Data;

@Data
public class ApprovingSaveRequest {
    private String fullName;
    private String email;
}
