package com.grant.freeGrant.other.request;

import lombok.Data;

@Data
public class IsAgreeRequest {
    private Long id;
}
