package com.grant.freeGrant.other.request;

import lombok.Data;
import org.antlr.v4.runtime.misc.NotNull;

@Data
public class StatusSaveRequest {
    @NotNull
    private String statusName;
}
