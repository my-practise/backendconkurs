package com.grant.freeGrant.other.request;

import com.grant.freeGrant.other.model.Course;
import lombok.Data;

@Data
public class CourseTypeSaveRequest {
    private String typeName;
    private Course course;
}
