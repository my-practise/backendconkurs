package com.grant.freeGrant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FreeGrantApplication {
	public static void main(String[] args) {
		SpringApplication.run(FreeGrantApplication.class, args);
	}

}
