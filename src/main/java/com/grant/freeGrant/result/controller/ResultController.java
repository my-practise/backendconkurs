package com.grant.freeGrant.result.controller;

import com.grant.freeGrant.faculty.model.Faculty;
import com.grant.freeGrant.result.model.Result;
import com.grant.freeGrant.result.service.ResultService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/result")
@RequiredArgsConstructor
public class ResultController {
    private final ResultService resultService;

    @PostMapping("/resultSave")
    public void resultSave(){
        resultService.resultSave();
    }

    @GetMapping("/listAll/{id}")
    public List<Result> listAll(@PathVariable Long id){
        return resultService.listAll(id);
    }
}
