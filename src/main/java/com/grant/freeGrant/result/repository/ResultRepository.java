package com.grant.freeGrant.result.repository;
import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.other.model.Approving;
import com.grant.freeGrant.result.model.Result;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ResultRepository extends JpaRepository<Result, Long> {
    @Query("SELECT r FROM Result r WHERE r.vacantGrant = :vacantGrant")
    List<Result> findByVacantGrant(@Param("vacantGrant") VacantGrant vacantGrant);
}