package com.grant.freeGrant.result.model;

import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(name = "result")
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "score")
    private Double score;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "date")
    private Timestamp date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vacant_grant_id")
    private VacantGrant vacantGrant;
    // Геттеры и сеттеры
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }


    public VacantGrant getVacantGrant() {
        return vacantGrant;
    }

    public void setVacantGrant(VacantGrant vacantGrant) {
        this.vacantGrant = vacantGrant;
    }

}