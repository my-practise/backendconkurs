package com.grant.freeGrant.result.service;

import com.grant.freeGrant.applicants.model.Applicant;
import com.grant.freeGrant.applicants.repository.ApplicantRepository;
import com.grant.freeGrant.other.StatusEnum;
import com.grant.freeGrant.other.model.Status;
import com.grant.freeGrant.other.repository.ApprovingRepository;
import com.grant.freeGrant.other.repository.StatusRepository;
import com.grant.freeGrant.result.model.Result;
import com.grant.freeGrant.result.repository.ResultRepository;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import com.grant.freeGrant.vacantGrant.repository.VacantGrantRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ResultService {
    private final ResultRepository repository;
    private final VacantGrantRepository vacantGrantRepository;
    private final ApplicantRepository applicantRepository;
    private final ApprovingRepository approvingRepository;
    private final StatusRepository statusRepository;

    @Transactional
    public void resultSave() {
        Status statusNew = statusRepository.findById(StatusEnum.NEW.getId()).orElse(null);
        List<VacantGrant> vacantGrant = vacantGrantRepository.findByStatusId(statusNew);
        List<Result> results = new ArrayList<>();
        for (VacantGrant grant : vacantGrant){
            List<Applicant> applicants = applicantRepository.findByVacantGrantId(grant);
            for (Applicant applicant : applicants){
                Double score = 0.0;
                Double countIsAgree = approvingRepository.getCountIsAgree(applicant);
                score+=countIsAgree;
                Result result = new Result();
                result.setDate(Timestamp.valueOf(LocalDateTime.now()));
                result.setFullName(applicant.getFirstName() +" "+ applicant.getLastName());
                result.setVacantGrant(grant);
                result.setScore(score + applicant.getGpa());
                results.add(result);
            }
            Collections.sort(results, (r1, r2) -> Double.compare(r2.getScore(), r1.getScore()));// сортировка по убыванию

            for (int i = 0; i < grant.getCountVacantGrant(); i++) {
                if (i < results.size()-1){
                    repository.save(results.get(i));
                }else {
                    break;
                }
            }
            Status status = statusRepository.findById(StatusEnum.COMPLETED.getId()).orElse(null);
            vacantGrantRepository.updateVacantGrantBy(grant.getId(), status);
        }
    }

    public List<Result> listAll(Long id){
        try {
            VacantGrant vacantGrant = vacantGrantRepository.findById(id).orElse(null);
            List<Result> results = repository.findByVacantGrant(vacantGrant);
            return results;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }


}
