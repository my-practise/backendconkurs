package com.grant.freeGrant.specialization.request;

import com.grant.freeGrant.faculty.model.Faculty;
import com.grant.freeGrant.other.model.Course;
import lombok.Data;

@Data
public class SpecializationSaveRequest {
    private String name;
    private Faculty faculty;
    private String createAt;
    private Course course;
}
