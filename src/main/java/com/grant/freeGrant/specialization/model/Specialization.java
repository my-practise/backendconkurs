package com.grant.freeGrant.specialization.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.grant.freeGrant.other.model.Course;
import com.grant.freeGrant.faculty.model.Faculty;
import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
public class Specialization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "faculty_id")
    private Faculty faculty;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    @Column(name = "creator")
    private String creator;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    // Конструкторы
    public Specialization() {
    }

    public Specialization(String name, Faculty faculty, Timestamp creationDate, Course course,String creator, boolean isDeleted) {
        this.name = name;
        this.faculty = faculty;
        this.creationDate = creationDate;
        this.creator = creator;
        this.course = course;
        this.isDeleted = isDeleted;
    }

    // Геттеры и сеттеры
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Course getCourses() {
        return course;
    }

    public void setCourses(Course courses) {
        this.course = courses;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
