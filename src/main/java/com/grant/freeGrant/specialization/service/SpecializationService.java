package com.grant.freeGrant.specialization.service;

import com.grant.freeGrant.specialization.model.Specialization;
import com.grant.freeGrant.specialization.repository.SpecializationRepository;
import com.grant.freeGrant.specialization.request.SpecializationSaveRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@Service
public class SpecializationService {
    private final SpecializationRepository specializationRepository;

    public void save(SpecializationSaveRequest request){
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        specializationRepository.save(new Specialization(request.getName(),
                request.getFaculty(),
                now,
                request.getCourse(),
                request.getCreateAt(),
                false));
    }

    public List<Specialization> listAll(){
        return specializationRepository.findAll();
    }
}
