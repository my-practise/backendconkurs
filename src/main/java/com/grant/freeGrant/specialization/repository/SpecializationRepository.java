package com.grant.freeGrant.specialization.repository;

import com.grant.freeGrant.specialization.model.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecializationRepository extends JpaRepository<Specialization, Long> {
}
