package com.grant.freeGrant.specialization.controller;

import com.grant.freeGrant.specialization.model.Specialization;
import com.grant.freeGrant.specialization.request.SpecializationSaveRequest;
import com.grant.freeGrant.specialization.service.SpecializationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/specialization")
public class SpecializationController {
    private final SpecializationService specializationService;

    @PostMapping("/save")
    public void save(@RequestBody SpecializationSaveRequest request){
        specializationService.save(request);
    }

    @GetMapping("/listAll")
    public List<Specialization> listAll(){
        return specializationService.listAll();
    }
}
