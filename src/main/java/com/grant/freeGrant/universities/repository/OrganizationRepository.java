package com.grant.freeGrant.universities.repository;

import com.grant.freeGrant.universities.model.Universities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends JpaRepository <Universities, Long> {
}
