package com.grant.freeGrant.universities.request;

import lombok.Data;

@Data
public class OrganizationSaveRequest {
    private String organizationName;
}
