package com.grant.freeGrant.universities.service;


import com.grant.freeGrant.universities.model.Universities;
import com.grant.freeGrant.universities.repository.OrganizationRepository;
import com.grant.freeGrant.universities.request.OrganizationSaveRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class UniversitiesService {
    private final OrganizationRepository organizationRepository;

    public void save(OrganizationSaveRequest request){
        try {
            organizationRepository.save(new Universities(request.getOrganizationName()));
        }catch (Exception e){
           new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Universities>> listAll(){
        try {
            List<Universities> universities = new ArrayList<Universities>();

            organizationRepository.findAll().forEach(universities::add);
            if (universities.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(universities, HttpStatus.OK);
        }catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
