package com.grant.freeGrant.universities.model;

import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(name = "universities")
public class Universities {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "university_name")
    private String universityName;

    @Column(name = "create_at")
    private Timestamp createAt = new Timestamp(System.currentTimeMillis());

    @Column(name = "update_at")
    private Timestamp updateAt = new Timestamp(System.currentTimeMillis());

    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    public Universities (String universityName){
        this.universityName = universityName;
    }

    public Universities() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return universityName;
    }

    public void setOrganizationName(String universityName) {
        this.universityName = universityName;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    public Timestamp getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Timestamp updateAt) {
        this.updateAt = updateAt;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
