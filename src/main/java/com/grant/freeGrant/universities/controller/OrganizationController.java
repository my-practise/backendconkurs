package com.grant.freeGrant.universities.controller;


import com.grant.freeGrant.universities.model.Universities;
import com.grant.freeGrant.universities.request.OrganizationSaveRequest;
import com.grant.freeGrant.universities.service.UniversitiesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/university")
public class OrganizationController {
    private final UniversitiesService service;

    @PostMapping("/save")
    public void save(@RequestBody OrganizationSaveRequest request){
        service.save(request);
    }

    @GetMapping("/listAll")
    public ResponseEntity<List<Universities>> listAll(){
        return service.listAll();
    }


}
