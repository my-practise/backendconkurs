package com.grant.freeGrant.vacantGrant.controller;

import com.grant.freeGrant.vacantGrant.dto.VacantGrantSaveRequest;
import com.grant.freeGrant.vacantGrant.dto.ListVacantGrants;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import com.grant.freeGrant.vacantGrant.service.VacantGrantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vacantGrants")
@RequiredArgsConstructor
public class VacantGrantController {

    private final VacantGrantService service;

    @PostMapping("/save")
    public ResponseEntity<VacantGrant> saveVacantGrant(@RequestBody VacantGrantSaveRequest vacantGrant) {
        return ResponseEntity.ok(service.saveVacantGrant(vacantGrant));
    }

    @GetMapping("/listAll")
    public ResponseEntity<ListVacantGrants> listAllVacantGrants() {
        return ResponseEntity.ok(service.listAllVacantGrants());
    }
}