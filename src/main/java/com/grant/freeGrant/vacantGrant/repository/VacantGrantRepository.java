package com.grant.freeGrant.vacantGrant.repository;

import com.grant.freeGrant.other.model.Status;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VacantGrantRepository extends JpaRepository<VacantGrant, Long> {
    @Query("SELECT v FROM VacantGrant v WHERE v.statusId = :statusId")
    List<VacantGrant> findByStatusId(@Param("statusId") Status statusId);

    @Modifying
    @Query("UPDATE VacantGrant v SET v.statusId = :statusId WHERE v.id = :id")
    int updateVacantGrantBy(Long id, Status statusId);

}