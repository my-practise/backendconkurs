package com.grant.freeGrant.vacantGrant.service;

import com.grant.freeGrant.applicants.dto.ApplicantListDto;
import com.grant.freeGrant.applicants.repository.ApplicantRepository;
import com.grant.freeGrant.other.StatusEnum;
import com.grant.freeGrant.other.model.Status;
import com.grant.freeGrant.other.repository.StatusRepository;
import com.grant.freeGrant.specialization.model.Specialization;
import com.grant.freeGrant.specialization.repository.SpecializationRepository;
import com.grant.freeGrant.vacantGrant.dto.VacantGrantSaveRequest;
import com.grant.freeGrant.vacantGrant.dto.ListVacantGrants;
import com.grant.freeGrant.vacantGrant.dto.VacantGrantDto;
import com.grant.freeGrant.vacantGrant.model.VacantGrant;
import com.grant.freeGrant.vacantGrant.repository.VacantGrantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VacantGrantService {

    private final VacantGrantRepository repository;
    private final ApplicantRepository applicantRepository;
    private final StatusRepository statusRepository;
    private final SpecializationRepository specializationRepository;

    public VacantGrant saveVacantGrant(VacantGrantSaveRequest vacantGrant) {
        Status status = statusRepository.findById(StatusEnum.NEW.getId()).orElseThrow(() -> new RuntimeException("Status not found"));
        Specialization specialization = specializationRepository.findById(vacantGrant.getSpecializationId()).orElseThrow(() -> new RuntimeException("Specialization not found"));

        VacantGrant vacantGrant1 = new VacantGrant();
        vacantGrant1.setCountVacantGrant(vacantGrant.getCountVacantGrant());
        vacantGrant1.setCourse(vacantGrant.getCourse());
        vacantGrant1.setCreateBy(vacantGrant.getCreateBy());
        vacantGrant1.setStatusId(status);
        vacantGrant1.setDeleted(false);
        vacantGrant1.setCreateTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        vacantGrant1.setSpecializationId(specialization);

        return repository.save(vacantGrant1);
    }

    public ListVacantGrants listAllVacantGrants() {
        ListVacantGrants list = new ListVacantGrants();
        list.setVacantGrants(repository.findAll().stream().map(vacantGrant ->
            new VacantGrantDto(
                    vacantGrant.getId(),
                    vacantGrant.getSpecialization().getFaculty().getFacultyName(),
                    vacantGrant.getSpecialization().getName(),
                    vacantGrant.getCountVacantGrant(),
                    vacantGrant.getStatus(),
                    vacantGrant.getCourse()
            )
        ).collect(Collectors.toList()));
        list.setApplicants( applicantRepository.findAll().stream().map(a ->
                new ApplicantListDto(
                        a.getVacantGrant().getId(),
                        a.getId(),
                        a.getUserId(),
                        a.getFirstName(),
                        a.getLastName(),
                        a.getIin(),
                        a.getGpa()
                )
        ).collect(Collectors.toList()));
        return list;
    }
}