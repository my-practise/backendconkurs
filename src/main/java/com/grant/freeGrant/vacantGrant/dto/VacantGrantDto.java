package com.grant.freeGrant.vacantGrant.dto;

import com.grant.freeGrant.other.model.Status;
import lombok.Data;

@Data
public class VacantGrantDto {
    private Long vacantGrantId;
    private String facultyName;
    private String specializationName;
    private Integer count;
    private Status status;
    private String course;

    public VacantGrantDto(Long vacantGrantId, String facultyName, String specializationName, Integer count, Status status, String course) {
        this.vacantGrantId = vacantGrantId;
        this.facultyName = facultyName;
        this.specializationName = specializationName;
        this.count = count;
        this.status = status;
        this.course = course;
    }
}
