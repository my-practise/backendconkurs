package com.grant.freeGrant.vacantGrant.dto;

import com.grant.freeGrant.applicants.dto.ApplicantListDto;
import lombok.Data;

import java.util.List;

@Data
public class ListVacantGrants {
    private List<VacantGrantDto> vacantGrants;
    private List<ApplicantListDto> applicants;
}
