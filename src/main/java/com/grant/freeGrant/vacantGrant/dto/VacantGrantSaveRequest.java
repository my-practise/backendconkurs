package com.grant.freeGrant.vacantGrant.dto;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.Data;

@Data
public class VacantGrantSaveRequest {
    private Long specializationId;
    private Integer countVacantGrant;
    private String createBy;
    private String course;
}
