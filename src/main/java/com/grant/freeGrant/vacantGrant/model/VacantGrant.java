package com.grant.freeGrant.vacantGrant.model;

import com.grant.freeGrant.other.model.Status;
import com.grant.freeGrant.specialization.model.Specialization;
import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(name = "vacant_grants")
public class VacantGrant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "specialization_id")
    private Specialization specialization;

    @Column(name = "count_vacant_grant")
    private Integer countVacantGrant;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status statusId;

    @Column(name = "create_timestamp")
    private Timestamp createTimestamp;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "course")
    private String course;

    public VacantGrant(){}
    public VacantGrant(Specialization specialization, Integer countVacantGrant, Status statusId, Timestamp createTimestamp, String createBy, Boolean isDeleted, String course) {
        this.specialization = specialization;
        this.countVacantGrant = countVacantGrant;
        this.statusId = statusId;
        this.createTimestamp = createTimestamp;
        this.createBy = createBy;
        this.isDeleted = isDeleted;
        this.course = course;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecializationId(Specialization specialization) {
        this.specialization = specialization;
    }

    public Integer getCountVacantGrant() {
        return countVacantGrant;
    }

    public void setCountVacantGrant(Integer countVacantGrant) {
        this.countVacantGrant = countVacantGrant;
    }

    public Status getStatus() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    public Timestamp getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Timestamp createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }


    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

}