package com.grant.freeGrant.faculty.request;

import lombok.Data;

@Data
public class FacultySaveRequest {
    private String facultyName;
    private long universitiesId;
}
