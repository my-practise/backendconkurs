package com.grant.freeGrant.faculty.controller;

import com.grant.freeGrant.faculty.model.Faculty;
import com.grant.freeGrant.faculty.request.FacultySaveRequest;
import com.grant.freeGrant.faculty.service.FacultyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@Controller
@RestController
@RequestMapping("/faculty")
public class FacultyController {
    private final FacultyService service;

    @PostMapping("/save")
    public void save(@RequestBody FacultySaveRequest request){
        service.save(request);
    }

    @GetMapping("/listAll")
    public ResponseEntity<List<Faculty>> listAll(){
        return service.listAll();
    }
}
