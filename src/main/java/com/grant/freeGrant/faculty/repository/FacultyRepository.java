package com.grant.freeGrant.faculty.repository;

import com.grant.freeGrant.faculty.model.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacultyRepository extends JpaRepository<Faculty, Long> {

}
