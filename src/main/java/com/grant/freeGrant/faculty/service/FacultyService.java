package com.grant.freeGrant.faculty.service;

import com.grant.freeGrant.faculty.model.Faculty;
import com.grant.freeGrant.faculty.repository.FacultyRepository;
import com.grant.freeGrant.faculty.request.FacultySaveRequest;
import com.grant.freeGrant.universities.model.Universities;
import com.grant.freeGrant.universities.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class FacultyService {
    private final FacultyRepository repository;
    private final OrganizationRepository organizationRepository;

    public void save (FacultySaveRequest request){
        Universities universities = organizationRepository.getReferenceById(request.getUniversitiesId());
        if (universities == null){
            throw new IllegalArgumentException ("Не найдено организаций");
        }
        repository.save(new Faculty(request.getFacultyName(), universities));
    }

    public ResponseEntity<List<Faculty>> listAll(){
        try {
            List<Faculty> faculties = new ArrayList<>();

            repository.findAll().forEach(faculties::add);
            if (faculties.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(faculties, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
